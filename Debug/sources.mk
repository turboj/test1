################################################################################
# 自动生成的文件。不要编辑！
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJS := 
SECONDARY_FLASH := 
SECONDARY_LIST := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
applications \
board \
libcpu/cpu \
libraries/hal_drivers \
libraries/hal_libraries/ab32vg1_hal/source \
libraries/hal_libraries/bmsis/source \
packages/bluetrum_sdk-latest/bluetrum_nimble/apps/blehr/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/ans/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/bas/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/dis/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/gap/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/gatt/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/ias/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/ipss/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/lls/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/services/tps/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/store/ram/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/util/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/porting/nimble/src \
packages/bluetrum_sdk-latest/bluetrum_nimble/porting/npl/rtthread/src \
rt-thread/components/drivers/misc \
rt-thread/components/drivers/serial \
rt-thread/components/drivers/src \
rt-thread/components/finsh \
rt-thread/components/libc/compilers/common \
rt-thread/components/libc/compilers/newlib \
rt-thread/src \

