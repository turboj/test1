################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal.c \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_dac.c \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_gpio.c \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_rcu.c \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_sd.c \
../libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_uart.c 

OBJS += \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal.o \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_dac.o \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_gpio.o \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_rcu.o \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_sd.o \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_uart.o 

C_DEPS += \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal.d \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_dac.d \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_gpio.d \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_rcu.d \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_sd.d \
./libraries/hal_libraries/ab32vg1_hal/source/ab32vg1_hal_uart.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/hal_libraries/ab32vg1_hal/source/%.o: ../libraries/hal_libraries/ab32vg1_hal/source/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include\libc" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\apps\blehr\src" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\ext\tinycrypt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ans\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\bas\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\dis\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gap\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gatt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ias\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ipss\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\lls\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\tps\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\store\ram\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\util\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\npl\rtthread\include" -I"D:\RT-ThreadStudio\workspace\test1" -I"D:\RT-ThreadStudio\workspace\test1\applications" -I"D:\RT-ThreadStudio\workspace\test1\board\ports" -I"D:\RT-ThreadStudio\workspace\test1\board" -I"D:\RT-ThreadStudio\workspace\test1\libcpu\cpu" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers\config" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include" -isystem"D:\RT-ThreadStudio\workspace\test1" -include"D:\RT-ThreadStudio\workspace\test1\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore  -ffunction-sections -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

