################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../rt-thread/src/clock.c \
../rt-thread/src/components.c \
../rt-thread/src/device.c \
../rt-thread/src/idle.c \
../rt-thread/src/ipc.c \
../rt-thread/src/irq.c \
../rt-thread/src/kservice.c \
../rt-thread/src/mem.c \
../rt-thread/src/memheap.c \
../rt-thread/src/mempool.c \
../rt-thread/src/object.c \
../rt-thread/src/scheduler.c \
../rt-thread/src/thread.c \
../rt-thread/src/timer.c 

OBJS += \
./rt-thread/src/clock.o \
./rt-thread/src/components.o \
./rt-thread/src/device.o \
./rt-thread/src/idle.o \
./rt-thread/src/ipc.o \
./rt-thread/src/irq.o \
./rt-thread/src/kservice.o \
./rt-thread/src/mem.o \
./rt-thread/src/memheap.o \
./rt-thread/src/mempool.o \
./rt-thread/src/object.o \
./rt-thread/src/scheduler.o \
./rt-thread/src/thread.o \
./rt-thread/src/timer.o 

C_DEPS += \
./rt-thread/src/clock.d \
./rt-thread/src/components.d \
./rt-thread/src/device.d \
./rt-thread/src/idle.d \
./rt-thread/src/ipc.d \
./rt-thread/src/irq.d \
./rt-thread/src/kservice.d \
./rt-thread/src/mem.d \
./rt-thread/src/memheap.d \
./rt-thread/src/mempool.d \
./rt-thread/src/object.d \
./rt-thread/src/scheduler.d \
./rt-thread/src/thread.d \
./rt-thread/src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
rt-thread/src/%.o: ../rt-thread/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include\libc" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\apps\blehr\src" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\ext\tinycrypt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ans\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\bas\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\dis\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gap\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gatt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ias\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ipss\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\lls\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\tps\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\store\ram\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\util\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\npl\rtthread\include" -I"D:\RT-ThreadStudio\workspace\test1" -I"D:\RT-ThreadStudio\workspace\test1\applications" -I"D:\RT-ThreadStudio\workspace\test1\board\ports" -I"D:\RT-ThreadStudio\workspace\test1\board" -I"D:\RT-ThreadStudio\workspace\test1\libcpu\cpu" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers\config" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include" -isystem"D:\RT-ThreadStudio\workspace\test1" -include"D:\RT-ThreadStudio\workspace\test1\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore  -ffunction-sections -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

