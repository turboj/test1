################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_decrypt.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_encrypt.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cbc_mode.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ccm_mode.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cmac_mode.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_mode.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_prng.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dh.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dsa.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_platform_specific.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac_prng.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/sha256.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/utils.c 

OBJS += \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_decrypt.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_encrypt.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cbc_mode.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ccm_mode.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cmac_mode.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_mode.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_prng.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dh.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dsa.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_platform_specific.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac_prng.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/sha256.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/utils.o 

C_DEPS += \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_decrypt.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/aes_encrypt.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cbc_mode.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ccm_mode.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/cmac_mode.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_mode.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ctr_prng.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dh.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_dsa.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/ecc_platform_specific.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/hmac_prng.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/sha256.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/utils.d 


# Each subdirectory must supply rules for building sources it contributes
packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/%.o: ../packages/bluetrum_sdk-latest/bluetrum_nimble/ext/tinycrypt/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include\libc" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\apps\blehr\src" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\ext\tinycrypt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ans\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\bas\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\dis\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gap\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gatt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ias\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ipss\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\lls\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\tps\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\store\ram\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\util\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\npl\rtthread\include" -I"D:\RT-ThreadStudio\workspace\test1" -I"D:\RT-ThreadStudio\workspace\test1\applications" -I"D:\RT-ThreadStudio\workspace\test1\board\ports" -I"D:\RT-ThreadStudio\workspace\test1\board" -I"D:\RT-ThreadStudio\workspace\test1\libcpu\cpu" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers\config" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include" -isystem"D:\RT-ThreadStudio\workspace\test1" -include"D:\RT-ThreadStudio\workspace\test1\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore  -ffunction-sections -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

