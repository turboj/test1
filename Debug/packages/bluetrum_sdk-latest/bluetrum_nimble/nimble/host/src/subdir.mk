################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_clt.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_cmd.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_svr.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_eddystone.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gap.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gattc.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts_lcl.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_adv.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_atomic.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_cfg.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_conn.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_flow.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_cmd.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_evt.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_util.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_id.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_log.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mbuf.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_misc.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mqueue.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_periodic_sync.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_pvcy.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_shutdown.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_startup.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_stop.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_ibeacon.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_coc.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig_cmd.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_monitor.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_alg.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_cmd.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_lgcy.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_sc.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store_util.c \
../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_uuid.c 

OBJS += \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_clt.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_cmd.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_svr.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_eddystone.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gap.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gattc.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts_lcl.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_adv.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_atomic.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_cfg.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_conn.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_flow.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_cmd.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_evt.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_util.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_id.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_log.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mbuf.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_misc.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mqueue.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_periodic_sync.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_pvcy.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_shutdown.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_startup.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_stop.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_ibeacon.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_coc.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig_cmd.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_monitor.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_alg.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_cmd.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_lgcy.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_sc.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store_util.o \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_uuid.o 

C_DEPS += \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_clt.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_cmd.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_att_svr.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_eddystone.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gap.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gattc.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_gatts_lcl.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_adv.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_atomic.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_cfg.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_conn.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_flow.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_cmd.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_evt.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_hci_util.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_id.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_log.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mbuf.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_misc.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_mqueue.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_periodic_sync.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_pvcy.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_shutdown.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_startup.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_hs_stop.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_ibeacon.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_coc.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_l2cap_sig_cmd.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_monitor.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_alg.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_cmd.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_lgcy.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_sm_sc.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_store_util.d \
./packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/ble_uuid.d 


# Each subdirectory must supply rules for building sources it contributes
packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/%.o: ../packages/bluetrum_sdk-latest/bluetrum_nimble/nimble/host/src/%.c
	riscv64-unknown-elf-gcc  -DDEBUG -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include\libc" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\apps\blehr\src" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\ext\tinycrypt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ans\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\bas\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\dis\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gap\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\gatt\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ias\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\ipss\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\lls\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\services\tps\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\store\ram\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\host\util\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\nimble\include" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\bluetrum_nimble\porting\npl\rtthread\include" -I"D:\RT-ThreadStudio\workspace\test1" -I"D:\RT-ThreadStudio\workspace\test1\applications" -I"D:\RT-ThreadStudio\workspace\test1\board\ports" -I"D:\RT-ThreadStudio\workspace\test1\board" -I"D:\RT-ThreadStudio\workspace\test1\libcpu\cpu" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers\config" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_drivers" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\ab32vg1_hal" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis\include" -I"D:\RT-ThreadStudio\workspace\test1\libraries\hal_libraries\bmsis" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest\blue_driver" -I"D:\RT-ThreadStudio\workspace\test1\packages\bluetrum_sdk-latest" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\drivers\include" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\finsh" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\common" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\components\libc\compilers\newlib" -I"D:\RT-ThreadStudio\workspace\test1\rt-thread\include" -isystem"D:\RT-ThreadStudio\workspace\test1" -include"D:\RT-ThreadStudio\workspace\test1\rtconfig_preinc.h" -std=gnu11 -c -mcmodel=medany -march=rv32imc -mabi=ilp32 -msave-restore  -ffunction-sections -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

